package com.example.bankapp.controller;

import com.example.bankapp.model.Account;
//import com.example.bankapp.model.Transaction;
import com.example.bankapp.service.AccountService;
//import com.example.bankapp.service.TransactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


import java.math.BigDecimal;
import java.util.List;

@RestController
@RequestMapping("/accounts")
public class AccountController {
    @Autowired
    private AccountService accountService;

   /* @Autowired
    private TransactionService transactionService;*/

    @PostMapping
    public Account createAccount(@RequestBody Account accountRequest) {
        return accountService.createAccount(accountRequest);
    }

    /*@GetMapping("/{accountNumber}")
    public Account getAccountByNumber(@PathVariable String accountNumber) {
        return accountService.getAccountByAccountNumber(accountNumber);
    }
*/
    /*@PutMapping("/{fromAccountNumber}/transfer/{toAccountNumber}")
    public void moveMoneyBetweenAccounts(@PathVariable String fromAccountNumber, @PathVariable String toAccountNumber, @RequestParam BigDecimal amount) {
        accountService.moveMoneyBetweenAccounts(fromAccountNumber, toAccountNumber, amount);
    }

    @PutMapping("/{fromAccountNumber}/pay/{toAccountNumber}")
    public void makePayment(@PathVariable String fromAccountNumber, @PathVariable String toAccountNumber, @RequestParam BigDecimal amount) {
        accountService.makePayment(fromAccountNumber, toAccountNumber, amount);
    }

    @GetMapping("/{accountNumber}/transactions")
    public List<Transaction> getTransactionsForAccount(@PathVariable Account accountNumber) {
        return transactionService.getTransactions(accountNumber);
    }*/
}

