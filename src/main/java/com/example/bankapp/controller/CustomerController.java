/*
package com.example.bankapp.controller;

import com.example.bankapp.model.Customer;
import com.example.bankapp.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/customers")
public class CustomerController {

    @Autowired
    private CustomerService customerService;

    @PostMapping
    public ResponseEntity<Customer> onboardCustomer(@RequestBody Customer customer) {
        Customer onboardedCustomer = customerService.createCustomer(customer);
        return ResponseEntity.ok(onboardedCustomer);
    }

}

*/
