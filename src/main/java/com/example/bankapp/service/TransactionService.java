/*
package com.example.bankapp.service;

import com.example.bankapp.model.Account;
import com.example.bankapp.model.Transaction;

import java.math.BigDecimal;
import java.util.List;

public interface TransactionService {
    Transaction deposit(Account account, BigDecimal amount);
    Transaction withdraw(Account account, BigDecimal amount);
    Transaction transfer(Account sourceAccount, Account destinationAccount, BigDecimal amount);
    Transaction payment(Account sourceAccount, Account destinationAccount, BigDecimal amount);
    List<Transaction> getTransactions(Account account);
}
*/
