/*
package com.example.bankapp.service;

import com.example.bankapp.model.Customer;
import com.example.bankapp.model.Transaction;
import org.springframework.stereotype.Service;

@Service
public class NotificationServiceImpl implements NotificationService {

    @Override
    public void sendNotification(Transaction transaction) {
        // get customer information from transaction
        Customer customer = transaction.getAccount().getCustomer();
        String customerName = customer.getName() + " " + customer.getLastName();
        String customerEmail = customer.getEmail();

        // prepare notification message
        String message = String.format("Dear %s,\n\nA transaction of %s ZAR has been made on your account with transaction ID %s.\n\nPlease contact us immediately if you did not authorize this transaction.\n\nThank you,\nBank X",
                customerName,
                transaction.getAmount().setScale(2),
                transaction.getId());

        // send notification email to customer
        sendEmail(customerEmail, "Transaction Notification", message);
    }

    private void sendEmail(String to, String subject, String message) {
        // code to send email to customer
    }
}
*/
