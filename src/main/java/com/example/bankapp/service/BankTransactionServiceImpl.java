package com.example.bankapp.service;/*
package za.co.bank.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import za.co.bank.model.Account;
import za.co.bank.model.Transaction;
import za.co.bank.repository.TransactionRepository;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;

@Service
public class BankTransactionServiceImpl implements BankTransactionService {

    @Autowired
    private TransactionRepository transactionRepository;
    @Autowired
    private AccountService accountService;

    @Override
    public List<Transaction> getBankTransactionsByDate(LocalDate date) {
        // Get all transactions that occurred on the given date
        return transactionRepository.findByTimestampBetween(date.atStartOfDay(), date.atTime(LocalTime.MAX));
    }

    @Override
    public void processBankTransactions(List<Transaction> transactions) {
        // Loop through the transactions and process each one
        for (Transaction transaction : transactions) {
            // Check if source and destination accounts exist
            Account sourceAccount = transaction.getAccount() != null ? accountService.getAccountByAccountNumber(transaction.getAccount().getId()) : null;
            Account destinationAccount = accountService.getAccountById(transaction.getDestinationAccountId());

            // Transfer funds between accounts
            accountService.transferFunds(sourceAccount, destinationAccount, transaction.getAmount());

            // Charge transaction fee to source account
            if (sourceAccount != null) {
                accountService.chargeTransactionFee(sourceAccount, transaction.getAmount());
            }

            // Credit interest to destination account
            accountService.creditInterest(destinationAccount);

            // Save the transaction
            transactionRepository.save(transaction);
        }
    }

}

*/
