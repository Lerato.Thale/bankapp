/*
package com.example.bankapp.service;
import com.example.bankapp.model.Account;
import com.example.bankapp.model.AccountType;
import com.example.bankapp.model.Transaction;
import com.example.bankapp.model.TransactionType;
import com.example.bankapp.repository.TransactionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

@Service
public class TransactionServiceImpl implements TransactionService {

    @Autowired
    private AccountService accountService;
    @Autowired
    private TransactionRepository transactionRepository;
    @Autowired
    private NotificationService notificationService;

    @Override
    @Transactional
    public Transaction deposit(Account account, BigDecimal amount) {
        BigDecimal newBalance = account.getBalance().add(amount);
        account.setBalance(newBalance);
        accountService.save(account);
        Transaction transaction = new Transaction(account, amount, LocalDateTime.now(), TransactionType.DEPOSIT);
        transactionRepository.save(transaction);
        notificationService.sendNotification(transaction);
        return transaction;
    }

    @Override
    @Transactional
    public Transaction withdraw(Account account, BigDecimal amount) {
        BigDecimal newBalance = account.getBalance().subtract(amount);
        account.setBalance(newBalance);
        accountService.save(account);
        Transaction transaction = new Transaction(account, amount.negate(), LocalDateTime.now(), TransactionType.WITHDRAWAL);
        transactionRepository.save(transaction);
        notificationService.sendNotification(transaction);
        return transaction;
    }

    @Override
    @Transactional
    public Transaction transfer(Account sourceAccount, Account destinationAccount, BigDecimal amount) {
        BigDecimal sourceNewBalance = sourceAccount.getBalance().subtract(amount);
        BigDecimal destinationNewBalance = destinationAccount.getBalance().add(amount);
        sourceAccount.setBalance(sourceNewBalance);
        destinationAccount.setBalance(destinationNewBalance);
        accountService.save(sourceAccount);
        accountService.save(destinationAccount);
        Transaction transaction = new Transaction(sourceAccount, amount.negate(), LocalDateTime.now(), TransactionType.TRANSFER);
        transaction.setAccount(destinationAccount);
        transactionRepository.save(transaction);
        notificationService.sendNotification(transaction);
        return transaction;
    }

    @Override
    @Transactional
    public Transaction payment(Account sourceAccount, Account destinationAccount, BigDecimal amount) {
        if (sourceAccount.getType() != AccountType.CURRENT) {
            throw new IllegalArgumentException("Only current accounts can make payments.");
        }
        BigDecimal sourceNewBalance = sourceAccount.getBalance().subtract(amount);
        sourceAccount.setBalance(sourceNewBalance);
        accountService.save(sourceAccount);
        Transaction transaction = new Transaction(sourceAccount, amount.negate(), LocalDateTime.now(), TransactionType.PAYMENT);
        transaction.setAccount(destinationAccount);
        transactionRepository.save(transaction);
        notificationService.sendNotification(transaction);
        return transaction;
    }

    @Override
    public List<Transaction> getTransactions(Account account) {
        return transactionRepository.findByAccountOrderByTimestampDesc(account);
    }
}
*/
