package com.example.bankapp.service;

import com.example.bankapp.model.Account;/*
import com.example.bankapp.model.AccountType;
import com.example.bankapp.model.Transaction;
import com.example.bankapp.model.TransactionType;*/
import com.example.bankapp.repository.AccountRepository;
//import com.example.bankapp.repository.TransactionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.math.BigDecimal;
import java.util.List;

@Service
public class AccountServiceImpl implements AccountService {
    @Autowired
    private AccountRepository accountRepository;
 /*   @Autowired
    private TransactionRepository transactionRepository;
*/
    @Override
    public Account createAccount(Account account) {
        // perform validations
        return accountRepository.save(account);
    }

   /* @Override
    public Account getAccountByAccountNumber(String accountNumber) {
        return accountRepository.findByAccountNumber(accountNumber).orElse(null);
    }

    @Override
    public List<Account> getAllAccounts() {
        return accountRepository.findAll();
    }*/

 /*   @Override
    public void moveMoneyBetweenAccounts(String fromAccountNumber, String toAccountNumber, BigDecimal amount) {
        // perform validations
        Account fromAccount = accountRepository.findByAccountNumber(fromAccountNumber).orElse(null);
        Account toAccount = accountRepository.findByAccountNumber(toAccountNumber).orElse(null);
        fromAccount.setBalance(fromAccount.getBalance().subtract(amount));
        toAccount.setBalance(toAccount.getBalance().add(amount));
        accountRepository.save(fromAccount);
        accountRepository.save(toAccount);
        Transaction debitTransaction = new Transaction();
        debitTransaction.setAccount(fromAccount);
        debitTransaction.setAmount(amount);
        debitTransaction.setType(TransactionType.DEBIT);
        transactionRepository.save(debitTransaction);
        Transaction creditTransaction = new Transaction();
        creditTransaction.setAccount(toAccount);
        creditTransaction.setAmount(amount);
        creditTransaction.setType(TransactionType.CREDIT);
        transactionRepository.save(creditTransaction);
    }*/

    /* @Override
    public void makePayment(String fromAccountNumber, String toAccountNumber, BigDecimal amount) {
        // perform validations
        Account fromAccount = accountRepository.findByAccountNumber(fromAccountNumber).orElse(null);
        fromAccount.setBalance(fromAccount.getBalance().subtract(amount.multiply(new BigDecimal("1.0005"))));
        accountRepository.save(fromAccount);
        Account toAccount = accountRepository.findByAccountNumber(toAccountNumber).orElse(null);
        toAccount.setBalance(toAccount.getBalance().add(amount));
        accountRepository.save(toAccount);
        Transaction debitTransaction = new Transaction();
        debitTransaction.setAccount(fromAccount);
        debitTransaction.setAmount(amount);
        debitTransaction.setType(TransactionType.DEBIT);
        transactionRepository.save(debitTransaction);
        Transaction creditTransaction = new Transaction();
        creditTransaction.setAccount(toAccount);
        creditTransaction.setAmount(amount);
        creditTransaction.setType(TransactionType.CREDIT);
        transactionRepository.save(creditTransaction);
    }*/

/*    @Override
    public void creditInterest(Account account, BigDecimal interest) {
        List<Account> savingsAccounts = accountRepository.findAllByAccountType(AccountType.SAVINGS);

        account.setBalance(account.getBalance().add(interest));
        accountRepository.save(account);
        Transaction creditTransaction = new Transaction();
        creditTransaction.setAccount(account);
        creditTransaction.setAmount(interest);
        creditTransaction.setType(TransactionType.CREDIT);
        transactionRepository.save(creditTransaction);

    }*/

/*    @Override
    public void processTransactions(List<Transaction> transactions) {
        for (Transaction transaction : transactions) {
            Account account = transaction.getAccount();
            if (transaction.getType() == TransactionType.CREDIT) {
                account.setBalance(account.getBalance().add(transaction.getAmount()));
            } else if (transaction.getType() == TransactionType.DEBIT) {
                account.setBalance(account.getBalance().subtract(transaction.getAmount()));
            }
            accountRepository.save(account);
            transactionRepository.save(transaction);
        }
    }*/

/*    @Override
    public List<Transaction> getAllTransactions() {
        return transactionRepository.findAll();
    }*/
}

