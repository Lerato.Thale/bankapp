package com.example.bankapp.model;

public enum AccountType {
    CURRENT,
    SAVINGS
}