package com.example.bankapp.model;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.List;

@Entity
@Table(name = "account")
public class Account {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long id;

   /* @ManyToOne
    @JoinColumn(name = "id")
    private Customer customer;*/

    @Enumerated(EnumType.STRING)
    private AccountType type;

    private BigDecimal balance;

    /*@OneToMany(mappedBy = "account", cascade = CascadeType.ALL)
    private List<Transaction> transactions;*/


    public Account() {
    }

    public Account(Long id/*, Customer customer*/, AccountType type, BigDecimal balance/*, List<Transaction> transactions*/) {
        this.id = id;
        /*this.customer = customer;*/
        this.type = type;
        this.balance = balance;
//        this.transactions = transactions;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    /*public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }*/

    public AccountType getType() {
        return type;
    }

    public void setType(AccountType type) {
        this.type = type;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

/*    public List<Transaction> getTransactions() {
        return transactions;
    }

    public void setTransactions(List<Transaction> transactions) {
        this.transactions = transactions;
    }*/
}