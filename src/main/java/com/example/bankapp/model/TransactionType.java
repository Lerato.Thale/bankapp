package com.example.bankapp.model;

public enum TransactionType {
    CREDIT,
    DEBIT,
    DEPOSIT,
    WITHDRAWAL,
    TRANSFER,
    PAYMENT
}
